#include <jni.h>
#include <stdlib.h>
#include <string.h>
#include "FTools/Icon.h"
#include "FTools/Iconcpp.h"
#include "imgui_switch.h"
#include <time.h>
#include "Includes.h"
ImGuiStyle *style;
int screenWidth = 0;
int screenHeight = 0;

bool g_Initialized = false;
ImGuiWindow *g_window = NULL;
ImGuiStyle *ref;
static int sliderint;

struct sConfig
{
    struct sESPMenu
    {
        bool Bones;
        bool 骨骼;
        bool 方框;
        bool 射线;
        bool 血量;
        bool 距离;
        bool 名字;
        bool 队伍;
        bool 信息;
        bool 背敌;
        bool 步枪;
        bool 吉普;
        bool 轿车;
        bool 摩托;
        bool 跑车;
        bool 游艇;
        bool 蹦蹦;
        bool 配件;
        bool 药品;
        bool 头盔;
        bool 背包;
        bool 倍镜;
        bool 功能一;
        bool 功能二;
        bool 功能三;
        bool 功能四;
        bool 功能五;
        bool 功能六;
        bool 开启绘制;
    };
    sESPMenu ESPMenu{
        0};

    struct sRadar
    {
        float x;
        float y;
    };
    sRadar Radar{
        0};
};
sConfig Config{
    0};

extern "C"
{
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_init(JNIEnv *env,
                                                                            jclass cls);
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_resize(JNIEnv *env,
                                                                              jobject obj,
                                                                              jint width,
                                                                              jint height);
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_step(JNIEnv *env,
                                                                            jobject obj);
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_imgui_Shutdown(JNIEnv *env,
                                                                                      jobject obj);
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_MotionEventClick(JNIEnv *
                                                                                            env,
                                                                                        jobject
                                                                                            obj,
                                                                                        jboolean
                                                                                            down,
                                                                                        jfloat
                                                                                            PosX,
                                                                                        jfloat
                                                                                            PosY);
    JNIEXPORT jstring JNICALL Java_com_mycompany_application_GLES3JNIView_getWindowRect(JNIEnv *
                                                                                            env,
                                                                                        jobject
                                                                                            thiz);
    JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_real(JNIEnv *env,
                                                                            jobject obj,
                                                                            jint width,
                                                                            jint height);
};

JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_init(JNIEnv *env, jclass cls)
{
    // 设置ImGui上下文
    if (g_Initialized)
        return;
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();

    io.IniFilename = NULL;

    // 设置ImGui风格
    // ImGui::StyleColorsDark(); // 设置主题[黑]
    // ImGui::StyleColorsClassic(); // 设置主题[紫]
    // ImGui::StyleColorsLight();	// 设置主题[白]
    ImGui::StyleColorsDark(); // 设置主题[蓝]

    ImGui_ImplAndroid_Init();
    ImGui_ImplOpenGL3_Init("#version 300 es");

    jfieldID Id = env->GetStaticFieldID(cls, "fontData", "[B");
    jbyteArray byteArray = (jbyteArray)env->GetStaticObjectField(cls, Id);
    jbyte *fontData = env->GetByteArrayElements(byteArray, NULL);
    int fontDataSize = env->GetArrayLength(byteArray);
    ImFont *font = io.Fonts->AddFontFromMemoryTTF(fontData, fontDataSize, 35.0f, NULL, io.Fonts->GetGlyphRangesChineseFull());
    IM_ASSERT(font != NULL);
    // 整体控件大小
    ImGui::GetStyle().ScaleAllSizes(0.7f);

    static const ImWchar icons_ranges[] = {0xf000, 0xf3ff, 0};
    ImFontConfig icons_config;

    ImFontConfig CustomFont;
    CustomFont.FontDataOwnedByAtlas = false;

    icons_config.MergeMode = true;
    icons_config.PixelSnapH = true;
    icons_config.OversampleH = 2.5;
    icons_config.OversampleV = 2.5;

    io.Fonts->AddFontFromMemoryCompressedTTF(font_awesome_data, font_awesome_size, 23.0f, &icons_config, icons_ranges);

    ImGui::SetNextWindowBgAlpha(5.0);
    ImGuiStyle &style = ImGui::GetStyle();
    // 内距
    style.ScaleAllSizes(1.5f);
    // 窗口菜单按钮位置(就是窗口标题的那个三角形)(-1无 0左 1右)
    style.WindowMenuButtonPosition = 0;
    // 窗体边框圆角
    style.WindowRounding = 20.0f;
    // 框架圆角(比如设置复选框的圆角)
    style.FrameRounding = 1.0f;
    // 框架描边宽度
    style.FrameBorderSize = 0.3f;
    // 滚动条圆角
    style.ScrollbarRounding = 5.0f;
    // 滚动条宽度
    style.ScrollbarSize = 40.0f;
    // 滑块圆角
    style.GrabRounding = 8.0f;
    // 滑块宽度
    style.GrabMinSize = 20.0f;

    g_Initialized = true;
}

JNIEXPORT void JNICALL
Java_com_mycompany_application_GLES3JNIView_resize(JNIEnv *env, jobject obj, jint width,
                                                   jint height)
{
    screenWidth = (int)width;
    screenHeight = (int)height;
    glViewport(0, 0, width, height);
    ImGuiIO &io = ImGui::GetIO();
    io.ConfigWindowsMoveFromTitleBarOnly = true;
    io.IniFilename = NULL;
    ImGui::GetIO().DisplaySize = ImVec2((float)width, (float)height);
}

/************自定义函数区**************/
static void MetricsHelpMarker(const char *desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

bool SetThemel(const char *label)
{
    static int style_id = 0;
    if (ImGui::Combo(label, &style_id, "主题紫\0主题黑\0主题白\0主题蓝\0"))
    {

        switch (style_id)

        {

        case 0:

            ImGui::StyleColorsClassic();

            break;
        case 1:

            ImGui::StyleColorsDark();

            break;
        case 2:
            ImGui::StyleColorsLight();

            break;
        case 3:
            ImGui::StyleColorsDark();
            break;
        }

        return true;
    }
    return false;
}

/************用户自定义**************/

static bool show_ChildMenu1 = true;
static bool show_ChildMenu2 = false;
static bool show_ChildMenu3 = false;
static bool show_ChildMenu4 = false;
static bool show_ChildMenu5 = false;

ImColor 方框颜色 = ImColor(225, 0, 0, 255);
ImColor 骨骼颜色 = ImColor(225, 0, 0, 255);
ImColor 射线颜色 = ImColor(225, 0, 0, 255);
ImColor 血条颜色 = ImColor(225, 0, 0, 255);
ImColor 人机颜色 = ImColor(225, 0, 0, 255);

void BeginDraw_Stayle()
{
    // UI窗体背景色
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(1.0, 1.0, 1.0, 0.95));
    style = &ImGui::GetStyle();
    style->WindowRounding = 5.0f;
    style->FrameRounding = 5.0f;
    style->GrabRounding = 100.0f;
    style->LogSliderDeadzone = 100.0f;
    style->WindowTitleAlign = ImVec2(0.5, 0.5);
    style->WindowMinSize = ImVec2(780, 440);
    style->Colors[ImGuiCol_TitleBg] = ImColor(255, 101, 53, 255);
    style->Colors[ImGuiCol_TitleBgActive] = ImColor(76, 125, 205, 200);
    style->Colors[ImGuiCol_TitleBgCollapsed] = ImColor(ImVec4(1.0, 1.0, 1.0, 0.55));
    style->Colors[ImGuiCol_Text] = ImColor(0, 0, 0, 255);
    style->Colors[ImGuiCol_Button] = ImColor(ImVec4(76 / 255.0, 125 / 255.0, 205 / 255.0, 200 / 255.0));
    style->Colors[ImGuiCol_ButtonActive] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_ButtonHovered] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_FrameBgActive] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_FrameBgHovered] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_Header] = ImVec4(76 / 255.0, 125 / 255.0, 205 / 255.0, 200 / 255.0);
    style->Colors[ImGuiCol_HeaderActive] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_HeaderHovered] = ImColor(ImVec4(0.619, 0.313, 0.685, 0.5));
    style->Colors[ImGuiCol_CheckMark] = ImVec4(1.0, 1.0, 1.0, 1.0);
}

static bool show;
void BeginDraw()
{
    ImGuiIO &io = ImGui::GetIO();

    // 设置下一个窗口的高宽
    ImGui::SetNextWindowSize(ImVec2((float)screenWidth * 0.75f, (float)screenHeight * 0.21f), ImGuiCond_Once);
    ImGui::Begin("fromsko", 0, ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_None);
    g_window = ImGui::GetCurrentWindow();
    static float values[90] = {};
    static int values_offset = 0;
    static double refresh_time = 0.0;
    while (refresh_time < ImGui::GetTime())
    {
        static float phase = 0.0f;
        values[values_offset] = cosf(phase);
        values_offset = (values_offset + 1) % IM_ARRAYSIZE(values);
        phase += 0.10f * values_offset;
        refresh_time += 1.0f / 120.0f;
    }
    char overlay[32] = "";
    ImGui::SameLine();
    ImGui::PlotLines("", values, IM_ARRAYSIZE(values), values_offset, overlay, -1.0f, 1.0f, ImVec2(0, 60.0f));
    ImGui::SameLine();
    ImGui::Text("FPS: %.2f", io.Framerate);
    ImGui::Separator();
    {
        if (ImGui::BeginChild("左侧主菜单", ImVec2(230, 0), true, ImGuiWindowFlags_None))
            ;
        {
            // if (ImGui::Button("人物模块",ImVec2(180, 60)))
            if (ImGui::Button(ICON_FA_USER_CIRCLE "人物模块", ImVec2(180, 60)))
            {
                show_ChildMenu1 = true;
                show_ChildMenu2 = false;
                show_ChildMenu3 = false;
                show_ChildMenu4 = false;
                show_ChildMenu5 = false;
            }
            if (ImGui::Button(ICON_FA_DROPBOX "载具模块", ImVec2(180, 60)))
            {
                show_ChildMenu1 = false;
                show_ChildMenu2 = true;
                show_ChildMenu3 = false;
                show_ChildMenu4 = false;
                show_ChildMenu5 = false;
            }
            if (ImGui::Button(ICON_FA_CROSSHAIRS "物资模块", ImVec2(180, 60)))
            {
                show_ChildMenu1 = false;
                show_ChildMenu2 = false;
                show_ChildMenu3 = true;
                show_ChildMenu4 = false;
                show_ChildMenu5 = false;
            }
            if (ImGui::Button(ICON_FA_GAMEPAD "内存模块", ImVec2(180, 60)))
            {
                show_ChildMenu1 = false;
                show_ChildMenu2 = false;
                show_ChildMenu3 = false;
                show_ChildMenu4 = true;
                show_ChildMenu5 = false;
            }
            if (ImGui::Button(ICON_FA_STAR "调节模块", ImVec2(180, 60)))
            {
                show_ChildMenu1 = false;
                show_ChildMenu2 = false;
                show_ChildMenu3 = false;
                show_ChildMenu4 = false;
                show_ChildMenu5 = true;
            }
            ImGui::Text("FPS: %0.2f", io.Framerate);
            ImGui::EndChild();
        }
        ImGui::SameLine();
        if (show_ChildMenu1)
        {
            if (ImGui::BeginChild("子菜单0", ImVec2(0, 0), true, ImGuiWindowFlags_None))
                ;
            {

                ImGui::SwitchButton("开启绘制", &Config.ESPMenu.开启绘制);

                if (ImGui::CollapsingHeader("绘制设置"))
                {
                    ImGui::Checkbox("骨骼", &Config.ESPMenu.骨骼);
                    ImGui::SameLine();
                    ImGui::Checkbox("方框", &Config.ESPMenu.方框);
                    ImGui::SameLine();
                    ImGui::Checkbox("射线", &Config.ESPMenu.射线);
                    ImGui::Checkbox("血量", &Config.ESPMenu.血量);
                    ImGui::SameLine();
                    ImGui::Checkbox("距离", &Config.ESPMenu.距离);
                    ImGui::SameLine();
                    ImGui::Checkbox("名字", &Config.ESPMenu.名字);
                    ImGui::Checkbox("队伍", &Config.ESPMenu.队伍);
                    ImGui::SameLine();
                    ImGui::Checkbox("信息", &Config.ESPMenu.信息);
                    ImGui::SameLine();
                    ImGui::Checkbox("背敌", &Config.ESPMenu.背敌);
                }
            }
            if (ImGui::CollapsingHeader("颜色设置"))
            {
                ImGui::ColorEdit4("方框颜色", (float *)&方框颜色);
                ImGui::ColorEdit4("血条颜色", (float *)&血条颜色);
                ImGui::ColorEdit4("射线颜色", (float *)&射线颜色);
                ImGui::ColorEdit4("骨骼颜色", (float *)&骨骼颜色);
                ImGui::ColorEdit4("人机颜色", (float *)&人机颜色);
            }
        }
        if (show_ChildMenu2)
        {
            if (ImGui::BeginChild("子菜单1", ImVec2(0, 0), true, ImGuiWindowFlags_None))
                ;
            {
                ImGui::Checkbox("吉普", &Config.ESPMenu.吉普);
                ImGui::SameLine();
                ImGui::Checkbox("轿车", &Config.ESPMenu.轿车);
                ImGui::SameLine();
                ImGui::Checkbox("蹦蹦", &Config.ESPMenu.蹦蹦);
                ImGui::Checkbox("摩托", &Config.ESPMenu.摩托);
                ImGui::SameLine();
                ImGui::Checkbox("跑车", &Config.ESPMenu.跑车);
                ImGui::SameLine();
                ImGui::Checkbox("游艇", &Config.ESPMenu.游艇);
            }
        }
        if (show_ChildMenu3)
        {
            if (ImGui::BeginChild("子菜单3", ImVec2(0, 0), true, ImGuiWindowFlags_None))
                ;
            {
                ImGui::Checkbox("枪械", &Config.ESPMenu.步枪);
                ImGui::SameLine();
                ImGui::Checkbox("配件", &Config.ESPMenu.配件);
                ImGui::SameLine();
                ImGui::Checkbox("药品", &Config.ESPMenu.药品);
                ImGui::Checkbox("倍镜", &Config.ESPMenu.倍镜);
                ImGui::SameLine();
                ImGui::Checkbox("背包", &Config.ESPMenu.背包);
                ImGui::SameLine();
                ImGui::Checkbox("头盔", &Config.ESPMenu.头盔);
            }
        }
        if (show_ChildMenu4)
        {
            if (ImGui::BeginChild("子菜单4", ImVec2(0, 0), true, ImGuiWindowFlags_None))
                ;
            {
                ImGui::Checkbox("功能一", &Config.ESPMenu.功能一);
                ImGui::SameLine();
                ImGui::Checkbox("功能二", &Config.ESPMenu.功能二);
                ImGui::SameLine();
                ImGui::Checkbox("功能三", &Config.ESPMenu.功能三);
                ImGui::Checkbox("功能四", &Config.ESPMenu.功能四);
                ImGui::SameLine();
                ImGui::Checkbox("功能五", &Config.ESPMenu.功能五);
                ImGui::SameLine();
                ImGui::Checkbox("功能六", &Config.ESPMenu.功能六);
                ImGui::SliderInt("SliderInt", &sliderint, 0, 100);
            }
        }
        if (show_ChildMenu5)
        {
            if (ImGui::BeginChild("子菜单5", ImVec2(0, 0), true, ImGuiWindowFlags_None))
                ;
            {
                ImGui::Text("颜色调节");
                SetThemel("主题风格##Selector");
                ImGui::Spacing();
            }
        }
    }
}

// 自定义图形绘制

// 结束
void EndDraw()
{
    ImGuiWindow *window = ImGui::GetCurrentWindow();
    window->DrawList->PushClipRectFullScreen();
    ImGui::End();
    // ImGui::PopStyleColor();
}

JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_step(JNIEnv *env, jobject obj)
{

    // ImGuiIO & io = ImGui::GetIO();

    static bool show_demo_window = false;
    static bool show_MainMenu_window = true;

    // 启动IMGUI框架
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplAndroid_NewFrame(screenWidth, screenHeight); // ？设置窗口
    ImGui::NewFrame();

    /* 用户自定义区域开始 */

    // 是否显示演示官方布局
    if (show_demo_window)
    {
        ImGui::ShowDemoWindow(&show_demo_window);
    }

    // 主菜单窗口
    if (show_MainMenu_window)
    {
        BeginDraw();
        BeginDraw_Stayle();
    }

    // 自定义图形绘制

    // 结束
    EndDraw();

    /* 用户自定义区域结束 */
    ImGui::Render();
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_imgui_Shutdown(JNIEnv *env,
                                                                                  jobject obj)
{
    if (!g_Initialized)
        return;
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplAndroid_Shutdown();
    ImGui::DestroyContext();
    g_Initialized = false;
}

JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_MotionEventClick(JNIEnv *
                                                                                        env,
                                                                                    jobject
                                                                                        obj,
                                                                                    jboolean
                                                                                        down,
                                                                                    jfloat
                                                                                        PosX,
                                                                                    jfloat PosY)
{
    ImGuiIO &io = ImGui::GetIO();
    io.MouseDown[0] = down;
    io.MousePos = ImVec2(PosX, PosY);
}

JNIEXPORT jstring JNICALL Java_com_mycompany_application_GLES3JNIView_getWindowRect(JNIEnv *
                                                                                        env,
                                                                                    jobject thiz)
{
    // 获取绘制窗口
    // TODO: 实现 getWindowSizePos()
    char result[256] = "0|0|0|0";
    if (g_window)
    {
        sprintf(result, "%d|%d|%d|%d", (int)g_window->Pos.x, (int)g_window->Pos.y, (int)g_window->Size.x, (int)g_window->Size.y);
    }
    return env->NewStringUTF(result);
}

JNIEXPORT void JNICALL Java_com_mycompany_application_GLES3JNIView_real(JNIEnv *env, jobject obj,
                                                                        jint w, jint h)
{
    screenWidth = (int)w;
    screenHeight = (int)h;
}
