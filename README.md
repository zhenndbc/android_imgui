# Android Imgui

> `Android imgui` 基础模板(公益收集),仅供学习研究.

## 构建内核

> 构建内核 => `app\src\main\jni`

`ndk-build`:

```shell
git clone https://github.com/ZTzTopia/GTInternalAndroid
cd GrowtopiaImGui
ndk-build
```

You need [ninja](https://ninja-build.org/) build system to use ninja cmake generator.

`cmake`:

```shell
mkdir build
cd build
cmake -GNinja -DCMAKE_MAKE_PROGRAM=/path/to/ninja.exe -DCMAKE_ANDROID_NDK=/path/to/android-ndk \
  -DCMAKE_TOOLCHAIN_FILE=toolchains/android.cmake ..
cmake --build .
```

## 二次开发

> 基于 `Android Studio` 构建的

1. UI 界面 `app\src\main\jni`
2. Java 层面 `app\src\main\java`

## 修复

- 修复多次启动导致崩溃
- 悬浮窗权限申请
- 修复启动闪退

## 待解决

- 安卓构建后，可能会被识别为恶意程序(实测没啥影响)
- 启动悬浮窗后，会造成上层覆盖
- 启动悬浮窗的时间有点小慢
- 暂不支持云端动态插件 `*.jar - *.so - clound`
- 暂未集成 `Luajava` 等
